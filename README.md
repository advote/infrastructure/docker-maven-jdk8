# Docker + OpenJDK8 + Maven container for gitlab ci building [![build status](https://gitlab.com/chvote-2.0/infra/docker-maven-oraclejdk8/badges/master/build.svg)](https://gitlab.com/chvote-2.0/infra/docker-maven-oraclejdk8/commits/master)

## Instructions
The container is based on an Alpine + OpenJDK 8 image, suitable for a CI environment. 
Docker and docker-compose are installed on this base image.

This image is meant to be used in the gitlab-ci pipelines of the CHVote applications.

## Content
* JDK 8.131.11
* Docker 17.05.00-ce
* Maven: latest
* Docker-compose: latest